using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPiece : MonoBehaviour
{
    public GameObject piece;
    Hambuilder hambuilder;
    public AudioClip sound;

    void Start(){
        hambuilder = GameObject.FindGameObjectWithTag("GameController").GetComponent<Hambuilder>();
    }

    public void AddToPile(){
        hambuilder.AddPiece(piece, sound);
    }
// Teste Queijo 0256546464684658468465468464
}
