﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hambuilder : MonoBehaviour
{
    List<GameObject> hamburger;
    List<AudioClip> soundsToPlay;

    public GameObject playButton;
    public GameObject deleteButton;

    //bool playAudio = false;
    int clipID = 0;


    public Transform plate;


    void Start(){
        hamburger = new List<GameObject>();
        soundsToPlay = new List<AudioClip>();
    }

    void Update(){
        /*if(playAudio){
            if(!audioSource.isPlaying){
                PlayAudio(clipID);
                clipID ++;
                if (clipID == hamburger.Count){
                    clipID = 0;
                    playAudio = false;
                }
            }
        } */
    }

    //------------------------------------------------------

    public void AddPiece(GameObject piece, AudioClip sound){
        if (hamburger.Count < 500){
            GameObject newPiece = Instantiate(piece, CheckPile(), transform.rotation);
            newPiece.GetComponent<AudioSource>().clip = sound;
            //newPiece.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
            //newPiece.transform.localScale = new Vector3(1,1,1);
            hamburger.Add(newPiece);
            soundsToPlay.Add(sound);
        }
    }

    public void RemovePiece(){
        if (hamburger.Count > 0){
            GameObject removedPiece = hamburger[hamburger.Count - 1];
            hamburger.Remove(hamburger[hamburger.Count - 1]);
            soundsToPlay.Remove(soundsToPlay[soundsToPlay.Count - 1]);
            Destroy(removedPiece);
        }
    }

    Vector2 CheckPile(){
        if (hamburger.Count == 0){
            return plate.position;
        } else{
            Transform lastPiece = (Transform)hamburger[hamburger.Count-1].transform;
            //return new Vector2(lastPiece.position.x, lastPiece.position.y + 0.5f); //lastPiece.GetComponent<Collider2D>().size.y
            return new Vector2 (plate.position.x, lastPiece.position.y + lastPiece.GetComponent<SpriteRenderer>().bounds.extents.y);
        }
    }

    //-------------------------------------------

    public void SayHamburger(){
        float delay = 0;
        /*playAudio = true;
        clipID = hamburger.Count - 1;
        DefineOutput();
        output.clip = soundsToPlay[clipID];
        output.Play();
        while(clipID > 0){
            clipID --;
            DefineOutput();
            output.clip = soundsToPlay[clipID];
            delay += output.clip.length;
            output.PlayDelayed(delay);
        }*/
        for (int piece = hamburger.Count - 1; piece >= 0; piece --){
            hamburger[piece].GetComponent<AudioSource>().PlayDelayed(delay -0.15f);
            delay += hamburger[piece].GetComponent<AudioSource>().clip.length;
        }
    }

    /*void PlayAudio(int clip){
        output.clip = soundsToPlay[clip];
        output.Play();
    } 

    void DefineOutput(){
        if (clipID %2 ==0){
            output = audiosource0;
        } else {
            output = audiosource1;
        }
    }*/

    public void Play_Button (){
        playButton.GetComponent<Animator>().Play("botao",0,0);
    }
    public void Delete_Button (){
        deleteButton.GetComponent<Animator>().Play("botao2",0,0);
    }

    //-----------------------------------------------------//


}
